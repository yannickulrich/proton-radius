# The proton size

When you look at the textbook entry on protons, you can see that its radius has been measured to be 0.84fm, that's roughly 100 billion times smaller than a human hair.
But how did scientists actually measure this?
And why did they care in the first place?

## How to measure the tiniest particles?

When you want to study small things like bacteria, you use a microscope.
So did the physicists measuring the proton radius just build a very big one and check?
The answer is yes, sort of.
A normal light-based microscope can only see things that are roughly 300 million times bigger than the proton.
Instead, they've built a small-ish particle accelerator and shoot electrons at the proton.
By measuring how electrons deflect, scientists can then reconstruct what the proton radius has to be.

The idea is quite similar to Rutherford's gold foil experiment which he and his students used to discover the atomic nucleus.
Rutherford scattered &alpha; particles (which we now know are Helium nuclei) off gold atoms and measured the scattering angle of the &alpha; particles.
The prevailing theory at the time assumed that atoms were small balls of positive charge with negatively charged electrons embedded like raisins in a plum pudding.
In this case, most of &alpha; particles would just pass through the gold foil, being only slightly deflected by the fairly weak electric field of the atom.
However, some particles were scattered much stronger, hinting at a much stronger field.
This implies a very small concentration of charge in the centre of the atoms -- the atomic nucleus.

These days, physicists use electrons rather than Helium nuclei for scattering experiments because electron beams from particle accelerators can be controlled much easier.
The principle of scattering off protons and other nuclei to determine their structure remains the same.

## Why do we care about the proton radius in the first place?

Many experiments have shown us that protons are not actually fundamental particles (instead, they are composed of three quarks).
So you might think that we know everything there is to know about the proton and that there is nothing to be gained from measuring the proton radius more precisely (other than academic credits).
However, there are a few reasons why we are still doing these experiments.

### Reason 1: we won't understand bigger nuclei if we don't understand the proton

The structure of the atomic nucleus is an important input for many areas of science and engineering.
Medical scanners like MRI and PET are based on nuclear physics.
So is nuclear power, both fission and fusion.
To make progress in these areas, we need to better understand the nuclei involved.
If we cannot understand the simplest of these, the proton, how can we hope to understand any larger atoms?

### Reason 2: studying fundamental physics

Our best understanding of the universe is the Standard Model of Particle Physics which we have tested to extremely high precision in many different experiments.
However, we also know that the Standard Model is not the ultimate theory of nature because it has some holes, among them:

 * it doesn't include gravity at all
 * it doesn't provide a candidate for Dark Matter.
    Dark Matter is a type of particle that physicists are sure exists from astronomical observation but that cannot be any of the known particles.
 * it doesn't explain where all the antimatter is.
    The Standard Model predicts that the matter and antimatter should've been created in equal amounts during the Big Bang.
    However, we cannot find any in the universe today.

Hence, we know there must be New Physics that we haven't seen yet.
Scientists are trying to find New Physics for example at the Large Hadron Collider at CERN near Geneva, Switzerland.
The LHC accelerates protons to extremely high energies and smashes them together, hoping to recreate conditions shortly after the Big Bang and to produce eg. Dark Matter particles.

However, there are other ways to look for New Physics and some of them involve the proton.
If there is a force additionally to the four that we know (electromagnetism, gravity, and the weak and strong nuclear forces) that couples to protons and electrons, it will influence how they interact.
To hunt for such an extra force, scientists measure the proton by scattering electrons and muons.
Muons (denoted by the Greek letter &mu;) are particles that behave like electrons in most ways -- except that they are two hundred times heavier.
In the Standard Model, this is the only difference between muons and electrons.
If scientists were to discover that muons and electrons scatter off protons differently, that would be clear evidence for New Physics because it could only be explained by a new force of nature.

**Historical side remark**

> This is not just hypothetical.
> In 2010, scientists at the Paul Scherrer Institute in Switzerland measured the proton radius using muons for the first time and found a value that was roughly 4% smaller than the previously accepted value.
> While 4% doesn't sound like a lot, it was *statistically significant* at roughly 5&sigma;.
> This means that the likelihood of getting this difference through bad luck alone is roughly 1 in 1.7 million.
> Over the next few years, the significance increase to roughly 7&sigma; or a chance of roughly 1 in 390 billion.
>
> This discrepancy has been dubbed the *Proton Radius Puzzle* and got a lot of scientists very excited.
> Could this finally be the hint for the new force we've been looking for?
> Unfortunately, it's probably wasn't.
> While not fully cleared up yet, some of the original experiments have been repeated and it looks like they agree with the measurements using muons.
